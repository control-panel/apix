# python
import re
# django
from django.core.validators import RegexValidator

def domainname_validator():

    return RegexValidator(
        r'[A-Za-z0-9]([A-Za-z0-9\-]{0,61}[A-Za-z0-9]?)', 
        message='Enter a valid Domain (Not a URL)', 
        code='invalid_domain'
    )