# python
from functools import reduce
# django
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin, messages
from django.urls import reverse
from django.utils.html import mark_safe
from django.core.exceptions import ValidationError
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.templatetags.static import static
# contrib
from adminsortable import admin as sortable
import nested_admin
# project
from . import models
# clone
from .clone_object import clone_object

class ConfigurationTranslationAdmin(admin.ModelAdmin):
    model = models.ConfigurationTranslation

    list_display = [
        'logo',
        'source',
        'lang',
    ]
    list_filter = (
        'source',
    )

    def logo(self, obj):
        return obj.source.logo


class FieldTranslationForm(forms.ModelForm):

    model = models.ConfigurationFieldTranslation
    fields = '__all__'

# In case we need to setup different settings according to widget type
def translation_form(parent):
    class FieldTranslationInline(FieldTranslationForm):
        def __init__(self, *args, **kwargs):
            widget = parent.widget
            super(FieldTranslationForm,self).__init__(*args, **kwargs)

    return FieldTranslationInline

class ConfigurationFieldTranslationInline(nested_admin.NestedStackedInline):
    model = models.ConfigurationFieldTranslation
    form  = FieldTranslationForm

    fields = (
        ( 'lang', 'label', ),
        ( 'helptext', 'default_value', )
    )

    def get_formset(self, request, obj=None, **kwargs):
        if obj:
            self.form = translation_form(obj)
        return super(ConfigurationFieldTranslationInline, self).get_formset(
            request,
            obj,
            **kwargs
        )

    extra = 0


class ConfigurationFieldTranslationAdmin(admin.ModelAdmin):

    model = models.ConfigurationFieldTranslation
    form  = FieldTranslationForm
    list_display = [
        'label',
        'app',
        'lang',
        'helptext',
    ]
    list_filter = (
        'source',
    )

    def app(self, obj):
        return obj.source.conf.conf.name

class ConfigurationFieldAdmin(admin.ModelAdmin):
    model = models.ConfigurationField
    list_display = [
        'label',
        'conf',
        'widget',
        'idiomas'
    ]
    list_filter = (
        'conf',
    )
    inlines = [
        ConfigurationFieldTranslationInline
    ]

    def label(self, obj):
        return obj

    def conf(self, obj):
        return obj.conf.conf

    def idiomas(self, obj):
        return [ i.lang for i in obj.translations.all() ]

class UserAdmin(admin.ModelAdmin):
    model = models.User

class ConfigurationFieldInline(nested_admin.NestedStackedInline):
    model = models.ConfigurationField
    show_change_link = True
    extra = 0
    fields = (
        ( 'widget', 'fid', 'required', 'required_if_installed', 'editable', 'link' ),
    )
    
    inlines = [ ConfigurationFieldTranslationInline, ]

class ReleaseInline(nested_admin.NestedTabularInline):
    
    model = models.Release.configurations.through
    extra = 0
    
class ConfigurationLinkInline(nested_admin.NestedTabularInline):
    
    model = models.ConfigurationLink
    extra = 0
    
class ConfigurationRemoveInfoInline(nested_admin.NestedTabularInline):
    
    model = models.ConfigurationRemoveInfo
    extra = 0

class ConfigurationVersionAdmin(nested_admin.NestedModelAdmin):

    model = models.ConfigurationVersion
    fields = (
        ('conf', 'version', 'remove'),
        ('linkapp', 'tomenu',),
        ('disk', 'ram',),
        ('categories', 'dependencies', 'users', ),
    )
    list_display = [
        'logo',
        'nombre',
        'remove',
        'campos',
        'dependencias',
        'usuarios',
        'releases_r',
        'enlaces',
        'HD',
        'RAM'
    ]
    list_filter = (
        'releases',
        'conf',
    )
    filter_horizontal = ('releases',)

    inlines = [ ReleaseInline, ConfigurationFieldInline, ConfigurationLinkInline, ConfigurationRemoveInfoInline ]

    actions = [ 'clone_configuration_version' ]

    def nombre(self, obj):
        return "%s %s" % ( obj.conf.name, obj.version )

    def logo(self, obj):
        return obj.conf.logo

    def campos(self, obj):
        if obj.fields:
            return [ f for f in obj.fields.all() ]
        return

    def dependencias(self, obj):
        if obj.dependencies:
            return [ dep.conf.name for dep in obj.dependencies.all() ]
        return

    def usuarios(self, obj):
        if obj.users:
            return [ user.name for user in obj.users.all() ]
        return
    
    def releases_r(self, obj):
        if obj.releases:
            return [ release.name for release in obj.releases.all() ]
        return
    
    def enlaces(self, obj):
        if obj.links:
            return [ link.url for link in obj.links.all() ]
        return

    def HD(self, obj):
        return str(obj.disk) + 'M'

    def RAM(self, obj):
        return str(obj.ram) + 'M'

    def clone_configuration_version(modeladmin, request, queryset):
        for obj in queryset:
            #print(obj.version+1.0) 
            clone_object(obj,{'version': obj.version+1.0})

    clone_configuration_version.short_description = _("Clone recursively")

class ConfigurationTranslationInline(nested_admin.NestedStackedInline):
    
    model = models.ConfigurationTranslation
    show_change_link = True
    extra = 0
    fields = (
        ( 'lang', 'name', ),
        ( 'link', 'link_text', 'link_target'),
        ( 'summary', 'description', )
    )
    
    inlines = [ ConfigurationFieldTranslationInline, ]


class ConfigurationAdmin(admin.ModelAdmin):

    list_display = [
        'logo',
        'name',
        'versiones',
    ]
    actions = [ 'add_new_version' ]

    inlines = [ ConfigurationTranslationInline, ]

    def versiones(self, obj):
        versions = models.ConfigurationVersion.objects.filter(conf=obj).order_by('-version')
        list = '<div>'
        for version in versions:
            list += '<p><a href="%s">%s %s</a></p>' % (
                version.get_absolute_url(),
                version.conf.name,
                version.version
            )
        list += '</div>'
        return mark_safe(list)

    def add_new_version(self, request, queryset):
        if len(queryset)>1:
            self.message_user(
                request,
                _('No puedes añadir más de una versión simultáneamente'),
                messages.ERROR
            )
            return
        conf = queryset[0]
        fields = models.ConfigurationField.objects.filter(
            conf=conf.last_version
        )
        new_version = models.ConfigurationVersion.objects.create(
            conf    = conf,
            version = conf.last_version.version + 0.1
        )
        new_version.save()
        for i in fields:
            newfield = models.ConfigurationField.objects.create(
                conf          = new_version,
                widget        = i.widget,
                required      = i.required
            )
            newfield.save()
        return HttpResponseRedirect( new_version.get_absolute_url() )

    add_new_version.short_description = _("Añade una versión nueva")


class ReleaseFieldsInline(admin.StackedInline):
    model = models.ReleaseFields
    show_change_link = True
    extra = 0

class ReleaseAdmin(admin.ModelAdmin):
    model = models.Release
    list_display = [
        'name',
        'active',
        'order',
        'status',
        'releases_compatibles',
        'vm',
        'apps',
        'api'
    ]
    list_filter = (
        'active',
        'status',
    )
    fields = (
        ('name', 'active',),
        ('status', 'order'),
        ('releases', 'configurations', 'updatedapps' ),
    )
    inlines = [ ReleaseFieldsInline ]

    def vm(self, obj):
        if obj.vm:
            return mark_safe( reduce(lambda a, b : a + "<a href='%s' target='_blank'>%s</a>, " % (
                    reverse('admin:models_vm_change', args=[b.pk]),
                    b.name
            ), obj.vm.all(), '' ) ) 
        return
    def releases_compatibles(self, obj):
        if obj.releases:
            return [ r for r in obj.releases.all() ]
        return
    def apps(self, obj):
        if obj.configurations:
            return mark_safe( reduce(lambda a, b : a + "<a href='%s' target='_blank'>%s</a>, " % (
                    reverse('admin:models_configurationversion_change', args=[b.pk]),
                    b.__str__()
            ), obj.configurations.all(), '' ) ) 
        return
    def api(self, obj):
        return mark_safe(
            "<a href='%s' target='_blank'><img src='%s' /></a>" % (
                reverse('api:releases-detail', args=[obj.name]),
                static('/apix/img/eye.svg')
            )
        )

class VMAdmin(admin.ModelAdmin):
    model = models.VM

    search_fields=('name','ip',)

    list_display = [
        'name',
        'ip',
        'status',
        'release',
        's3_enabled',
        's3_mastodon_enabled',
        's3_mastodon_usage',
        's3_peertube_enabled',
        's3_peertube_usage',
        'backup_enabled',
        'primary',
        'backup_server',
        'backup_server_2',
        'api'
    ]

    actions = ["switch_borg_primary_server"]

    #override column names in admin
    def primary(self, obj):
      return obj.backup_primary

    #action to switch borg primary server
    def switch_borg_primary_server(self, request, queryset):
       for obj in queryset:
         changed = 0
         #print (obj.backup_primary + 'kkkkkkkkkkkkkkkk')
         if obj.backup_primary == "1":
            obj.backup_primary = "2"
            obj.save(update_fields=['backup_primary'])
            changed = 1
         if obj.backup_primary == "2" and changed == 0:
            obj.backup_primary = "1"
            obj.save(update_fields=['backup_primary'])
    switch_borg_primary_server.short_description = "Switch Borg primary Server"


    list_filter = (
        'release',
        'status',
        'backup_enabled',
        's3_enabled',
        's3_mastodon_enabled',
        's3_peertube_enabled',
        'backup_primary',
        'backup_server',
        'backup_server_2',
    )
    fieldsets = (
        (None, {
            'fields': ('name','ip','status','release','users'),
        }),
        ('S3 conf', {
            'fields': ('s3_enabled','s3_mastodon_filesystem','s3_peertube_filesystem'),
        }),
        ('S3 Mastodon conf', {
            'fields': ('s3_mastodon_enabled','s3_mastodon_bucket','s3_mastodon_location','s3_mastodon_prefix','s3_mastodon_access_key','s3_mastodon_secret_key'),
        }),
        ('S3 Peertube conf', {
            'fields': ('s3_peertube_enabled','s3_peertube_bucket','s3_peertube_location','s3_peertube_prefix','s3_peertube_access_key','s3_peertube_secret_key'),
        }),
        ('General backup conf', {
            'fields': ('backup_enabled','backup_ssh_key','backup_directory','backup_primary','backup_port','backup_daily','backup_weekly','backup_monthly'),
        }),
        ('Backup server 1', {
            'fields': ('backup_server', 'backup_user'),
        }),
        ('Backup server 2', {
            'fields': ('backup_server_2', 'backup_user_2'),
        }),
        ('Backup server Old', {
            'fields': ('backup_server_old', 'backup_user_old'),
        }),
    )
    def api(self, obj):
        return mark_safe(
            "<a href='%s' target='_blank'><img src='%s' /></a>" % (
                reverse('api:vm-detail', args=[obj.name]),
                static('/apix/img/eye.svg')
            )
        )
    

# admin.site.register(models.VM, admin.ModelAdmin)
admin.site.register(models.Configuration, ConfigurationAdmin)
admin.site.register(models.ConfigurationVersion, ConfigurationVersionAdmin)
admin.site.register(models.ConfigurationTranslation, ConfigurationTranslationAdmin)
admin.site.register(models.ConfigurationField, ConfigurationFieldAdmin)
admin.site.register(models.ConfigurationFieldTranslation, ConfigurationFieldTranslationAdmin)
admin.site.register(models.User, UserAdmin)
admin.site.register(models.Release, ReleaseAdmin)
admin.site.register(models.ReleaseFields, admin.ModelAdmin)
admin.site.register(models.ConfigurationCategory, admin.ModelAdmin)
admin.site.register(models.VM, VMAdmin)
