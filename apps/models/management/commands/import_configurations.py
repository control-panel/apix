# python
import json, re
# django
from django.core.management.base import BaseCommand, CommandError
# project
from apps.models import models
from django.conf import settings

"""
A manage.py command to import configurations from a json file
"""

class Command(BaseCommand):

    """
    Adds the path to project resource
    """
    def add_arguments(self, parser):
        parser.add_argument(
            '--f',
            type=str,
            help='Provide a valid file',
        )

    """
    Imports Audioset
    """
    def handle(self, *args, **options):
        for c in models.Configuration.objects.all():
            c.delete()
        for c in models.User.objects.all():
            c.delete()
        filename = options['f'] if options['f'] else '../fixtures/configurations.json'
        json_file = open( filename )
        data = json.load( json_file )
        configurations = [ i for i in data if i['id'].endswith('es') or i['id'].endswith('en') ]
        dependent_apps = {}
        fields_lut = {}
        cat, created = models.ConfigurationCategory.objects.get_or_create(category = 'App')

        # Set-up configurations in a two-setep iteration
        # Create configuration objects
        for conf in configurations:
            image = 'https://assets.maadix.net/c-panel/images/services/'
            source, created = models.Configuration.objects.get_or_create(
                name = conf['name'],
                slug = conf['id'].replace('_es', '').replace('_en', '')
            )
            if 'img' in conf:
                source.logo_filename = conf['img']
                source.save()
            version, created = models.ConfigurationVersion.objects.get_or_create(conf = source)
            version.categories.add( cat )
            # Add translation
            langcode = conf['id'].split('_')[1]
            translation = {
                'lang'   : langcode,
                'source' : source
            }
            if 'title' in conf       : translation['summary']     = conf['title']
            if 'name' in conf        : translation['name']        = conf['name']
            if 'description' in conf : translation['description'] = conf['description']
            if 'link_url' in conf    : translation['link']        = conf['link_url']
            if 'link_text' in conf   : translation['link_text']   = conf['link_text']
            if 'target' in conf      : translation['link_target'] = conf['target']
            pk = models.ConfigurationTranslation.objects.create(**translation)

            if not source.name in fields_lut: fields_lut[ source.name ] = {}

        # Set up dependencies
        dependent_confs = [ c for c in configurations if 'dependencies' in c ]
        for conf in dependent_confs:
            appname = conf['name']
            app = models.Configuration.objects.get( name = appname )
            version = models.ConfigurationVersion.objects.get( conf = app )
            langcode = conf['id'].split('_')[1]
            for dep_pos, dependency in enumerate(conf['dependencies']):
                # field dependency lut
                # we rely on the position of the dependency in the list, it seems
                # there's no better way of acomplishing this
                dep_info = re.match('(.+)\[Description\:(.+)\]', dependency)

                # Dependencies with fields
                if dep_info or '.' in dependency and not 'sysuser' in dependency:
                    params = dep_info[1].split('.') if dep_info else dependency.split('.')
                    # Prevent from duplicating widgets
                    widget = {
                        'txt'    : 'text',
                        'domain' : 'url'
                    }[params[1]]
                    # Set source field
                    if not dep_pos in fields_lut[ appname ]:
                        fields_lut[ appname ][ dep_pos ] = models.ConfigurationField.objects.create(
                            fid      = params[0],
                            widget   = widget,
                            conf     = version,
                            required = True,
                        )
                    # Add translation
                    t = {
                        'lang'   : langcode,
                        'source' : fields_lut[ appname ][ dep_pos ],
                        'label'  : params[2],
                    }
                    if(dep_info): t['helptext'] = dep_info[2].strip()
                    models.ConfigurationFieldTranslation.objects.create(**t)
                else:
                    # Dependencies with other apps
                    if not '.' in dependency:
                        app = models.Configuration.objects.get(
                            name__iexact = dependency
                        ).versions.first()
                        version.dependencies.add( app )
                    # Dependencies with users
                    else:
                        user_dependency, created = models.User.objects.get_or_create(
                            name = dependency.replace('sysuser.', '')
                        )
                        version.users.add( user_dependency )
                    # version.save()
