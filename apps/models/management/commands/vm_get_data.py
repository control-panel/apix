from apps.models.models import VM
from django.core.management.base import BaseCommand
from django.forms.models import model_to_dict
from django.core import serializers

class Command(BaseCommand):
    help = 'Update VM'

    def add_arguments(self, parser):
        parser.add_argument('--name', type=str, help='Name')

    def handle(self, *args, **kwargs):
        name = kwargs['name']
        try:
          vm=VM.objects.get(name=name)
          print(serializers.serialize('json', [ vm, ]))
        except VM.DoesNotExist:
          raise CommandError('VM "%s" does not exist' % name)

