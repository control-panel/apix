from apps.models.models import VM
from django.core.management.base import BaseCommand
from django.forms.models import model_to_dict

class Command(BaseCommand):
    help = 'Update VM'

    def add_arguments(self, parser):
        parser.add_argument('--name', type=str, help='Name')
        parser.add_argument('--server1', type=str, help='Server 1')
        parser.add_argument('--server2', type=str, help='Server 2')
        parser.add_argument('--user1', type=str, help='User 1')
        parser.add_argument('--user2', type=str, help='User 2')
        parser.add_argument('--directory', type=str, help='Directory')

    def handle(self, *args, **kwargs):
        name = kwargs['name']
        server1 = kwargs['server1']
        server2 = kwargs['server2']
        user1 = kwargs['user1']
        user2 = kwargs['user2']
        directory = kwargs['directory']
        try:
          vm = VM.objects.filter(name=name)
          vm.update(backup_daily=7,backup_weekly=8,backup_monthly=4,backup_enabled=True,backup_server=server1,backup_user=user1,backup_server_2=server2,backup_user_2=user2,backup_directory=directory,backup_port=23)
          vm=VM.objects.get(name=name)
          print(model_to_dict(vm))
        except VM.DoesNotExist:
          raise CommandError('VM "%s" does not exist' % name)
