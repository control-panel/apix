import yaml
from apps.models.models import VM
from django.core.management.base import BaseCommand
from django.forms.models import model_to_dict

class Command(BaseCommand):
    help = 'Update S3 apps usage, data in MiB'

    def handle(self, *args, **kwargs):
        #get s3 apps sizes from file
        f = open('/tmp/s3_size_usage.yaml')
        s3_apps = yaml.safe_load(f)
        for name in s3_apps:
          #print(name)
          vm = VM.objects.filter(name=name)
          if 's3_mastodon_size' in s3_apps[name].keys():
            mastodon_usage = s3_apps[name]['s3_mastodon_size']
          else:
            mastodon_usage = 0
          if 's3_peertube_size' in s3_apps[name].keys():
            peertube_usage = s3_apps[name]['s3_peertube_size']
          else:
            peertube_usage = 0
          #print(mastodon_usage,peertube_usage)
          vm.update(s3_mastodon_usage=mastodon_usage,s3_peertube_usage=peertube_usage)
          vm=VM.objects.get(name=name)
          #print(model_to_dict(vm))
