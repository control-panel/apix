# python
import json, re
# django
from django.core.management.base import BaseCommand, CommandError
# project
from apps.models import models
from django.conf import settings

"""
A manage.py command to import configurations from a json file
"""

class Command(BaseCommand):

    """
    Adds the path to project resource
    """
    def add_arguments(self, parser):
        parser.add_argument(
            '--f',
            type=str,
            help='Provide a valid file',
        )

    """
    Imports Audioset
    """
    def handle(self, *args, **options):
        for r in models.Release.objects.all():
            r.delete()
        filename = options['f'] if options['f'] else '../fixtures/releases.json'
        json_file = open( filename )
        releases = json.load( json_file )

        # Set-up configurations in a two-setep iteration
        # Create configuration objects
        for release_data in releases:
            release = models.Release.objects.create(
                name        = release_data['release'],
                description = release_data['description'] if 'description' in release_data else None,
                order       = release_data['order'],
                active      = release_data['active']
            )
            release.save()
            for confname in release_data['groups']:
                confname = confname.replace("_es", "")
                confname = confname.replace("_en", "")
                conf = models.Configuration.objects.get(
                    slug=confname
                ).versions.first()
                release.configurations.add( conf )

        releases_with_compatibles = filter(lambda i : 'compatible' in i, releases)
        for release_data in releases_with_compatibles:
            release = models.Release.objects.get( name = release_data['release'] )
            for r in release_data['compatible']:
                release.releases.add(
                    models.Release.objects.get(name = r)
                )
