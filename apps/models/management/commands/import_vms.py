import json
from apps.users.models import Uxer
from apps.models.models import VM
from apps.models.models import Release
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    help = 'Import VMs'

    def add_arguments(self, parser):
        parser.add_argument('--json', type=str, help='Path to json file with vms')

    def handle(self, *args, **kwargs):
        jsonfile = kwargs['json']
        release = Release.objects.get(name='release_201902')
        print ('json: ' +jsonfile)
        vmsList = []
        print ("Started Reading JSON file which contains multiple JSON vms")
        with open(jsonfile) as f:
          for jsonObj in f:
            print (jsonObj)
            vmDict = json.loads(jsonObj)
            vmsList.append(vmDict)

        print ("Adding vms:")
        for vm in vmsList:
          print (vm["vmname"] + ' | ' + vm["vmip"])
          try:
            vmobj = VM.objects.get(name=vm["vmname"])
            print ('Existe vm ' + vm["vmname"] + ' / se descarta crear nueva')
          except VM.DoesNotExist:
            print ('No existe vm ' + vm["vmname"] + ' / se crea una nueva')
            newvm = VM(
              name        = vm["vmname"],
              ip          = vm["vmip"],
              status      = 0,
              release     = release,
            )
            newvm.save()
