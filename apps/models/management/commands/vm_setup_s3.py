from apps.models.models import VM
from django.core.management.base import BaseCommand
from django.forms.models import model_to_dict

class Command(BaseCommand):
    help = 'Update VM S3 conf'

    def add_arguments(self, parser):
        parser.add_argument('--name', type=str, help='Name')
        parser.add_argument('--app', type=str, help='App')
        parser.add_argument('--bucket', type=str, help='Bucket')
        parser.add_argument('--location', type=str, help='Location')
        parser.add_argument('--prefix', type=str, help='Prefix')
        parser.add_argument('--key', type=str, help='Access Key')
        parser.add_argument('--secret', type=str, help='Secret Key')

    def handle(self, *args, **kwargs):
        #get conf
        name = kwargs['name']
        app = kwargs['app']
        bucket = kwargs['bucket']
        location = kwargs['location']
        prefix = kwargs['prefix']
        key = kwargs['key']
        secret = kwargs['secret']
        try:
          vm = VM.objects.filter(name=name)
          if app == 'mastodon':
            vm.update(s3_enabled=True,s3_mastodon_enabled=True,s3_mastodon_bucket=bucket,s3_mastodon_location=location,s3_mastodon_prefix=prefix,s3_mastodon_access_key=key,s3_mastodon_secret_key=secret,s3_mastodon_filesystem=False)
          if app == 'peertube':
            vm.update(s3_enabled=True,s3_peertube_enabled=True,s3_peertube_bucket=bucket,s3_peertube_location=location,s3_peertube_prefix=prefix,s3_peertube_access_key=key,s3_peertube_secret_key=secret,s3_peertube_filesystem=False)
          vm=VM.objects.get(name=name)
          print(model_to_dict(vm))
        except VM.DoesNotExist:
          raise CommandError('VM "%s" does not exist' % name)
