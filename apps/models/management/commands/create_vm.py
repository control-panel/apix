from apps.users.models import Uxer
from apps.models.models import VM
from apps.models.models import Release
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    help = 'Create VM'

    def add_arguments(self, parser):
        parser.add_argument('--name', type=str, help='Name')
        parser.add_argument('--ip', type=str, help='Ip')
        parser.add_argument('--release', type=str, help='Release')

    def handle(self, *args, **kwargs):
        name = kwargs['name']
        ip = kwargs['ip']
        releasename = kwargs['release']
        #print ('name: ' + name)
        #print ('ip: ' + ip)
        #print ('releasename: ' + releasename)
        user = Uxer.objects.get(username=name)
        release = Release.objects.get(name=releasename)
        #delete vm if exists
        if VM.objects.filter(name=name).exists():
          v = VM.objects.get(name=name)
          v.delete()
        vm = VM(name=name, ip=ip, release=release, status=0)
        vm.save()
        vm.users.add(user)
