# django
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.utils.html import mark_safe
from django.templatetags.static import static
# contrib
from adminsortable.models import SortableMixin
from adminsortable.fields import SortableForeignKey
from ckeditor_uploader.fields import RichTextUploadingField
from django_dnf.fields import DomainNameField
from url_or_relative_url_field.fields import URLOrRelativeURLField
# project
from django.conf import settings
from .langs import LANGUAGES
from apps.users.models import Uxer
from .validators import domainname_validator


dn_validator = domainname_validator()

class VM(models.Model):
    """ App model definition """

    name = models.CharField(
        blank=False,
        null=True,
        max_length=63,
        validators=[ dn_validator ]
    )
    release = models.ForeignKey(
        'Release',
        null=True,
        blank=False,
        related_name='vm',
        on_delete=models.SET_NULL
    )
    ip = models.GenericIPAddressField(
        _('IP'),
        max_length=128,
        unique=True
    )
    status = models.CharField(
        _('Estado'),
        max_length=1,
        choices = (
            ('0', _('Ready')),
            ('1', _('Pending')),
            ('2', _('Error')),
            ('3', _('Upgrade'))
        )
    )
    users = models.ManyToManyField(
        Uxer,
        blank=True,
        verbose_name=_('Usuarios')
    )

    #S3 general
    s3_enabled = models.BooleanField(
        _('S3 enabled'),
        default=False,
        blank=False,
    )

    s3_mastodon_filesystem = models.BooleanField(
        _('Mastodon with FS'),
        default=False,
        blank=False,
    )

    s3_peertube_filesystem = models.BooleanField(
        _('Peertube with FS'),
        default=False,
        blank=False,
    )

    #S3 mastodon
    s3_mastodon_enabled = models.BooleanField(
        _('S3 Mastodon enabled'),
        default=False,
        blank=False,
    )

    s3_mastodon_bucket = models.CharField(
        _('S3 Mastodon bucket'),
        max_length=128,
        blank=True,
    )

    s3_mastodon_location = models.CharField(
        _('S3 Mastodon location'),
        max_length=128,
        blank=True,
    )

    s3_mastodon_prefix = models.CharField(
        _('S3 Mastodon prefix'),
        max_length=128,
        blank=True,
    )

    s3_mastodon_usage = models.PositiveIntegerField(
        _('S3 Mastodon usage'),
        blank=True,
        default=0,
    )

    s3_mastodon_access_key = models.CharField(
        _('S3 Mastodon access key'),
        max_length=128,
        blank=True,
    )

    s3_mastodon_secret_key = models.CharField(
        _('S3 Mastodon secret key'),
        max_length=128,
        blank=True,
    )

    #S3 peertube
    s3_peertube_enabled = models.BooleanField(
        _('S3 Peertube enabled'),
        default=False,
        blank=False,
    )

    s3_peertube_bucket = models.CharField(
        _('S3 Peertube bucket'),
        max_length=128,
        blank=True,
    )

    s3_peertube_location = models.CharField(
        _('S3 Peertube location'),
        max_length=128,
        blank=True,
    )

    s3_peertube_prefix = models.CharField(
        _('S3 Peertube prefix'),
        max_length=128,
        blank=True,
    )

    s3_peertube_usage = models.PositiveIntegerField(
        _('S3 Peertube usage'),
        blank=True,
        default=0,
    )

    s3_peertube_access_key = models.CharField(
        _('S3 Peertube access key'),
        max_length=128,
        blank=True,
    )

    s3_peertube_secret_key = models.CharField(
        _('S3 Peertube secret key'),
        max_length=128,
        blank=True,
    )


    #General backup fields
    backup_enabled = models.BooleanField(
        _('Backup enabled'),
        default=False,
        blank=False,
    )

    backup_directory = models.CharField(
        _('Backup directory'),
        max_length=128,
        blank=True,
    )

    backup_ssh_key = models.CharField(
        _('Backup ssh Public key'),
        max_length=1028,
        blank=True,
    )

    backup_primary = models.CharField(
        _('Servidor backup primario'),
        max_length=1,
        blank=False,
        default='1',
        choices = (
            ('1', _('Servidor 1')),
            ('2', _('Servidor 2'))
        )
    )

    backup_port = models.PositiveIntegerField(
        _('Backup port'),
        blank=True,
        default=22,
    )

    backup_daily = models.PositiveIntegerField(
        _('Backup daily'),
        blank=True,
        default=0,
    )

    backup_weekly = models.PositiveIntegerField(
        _('Backup weekly'),
        blank=True,
        default=0,
    )

    backup_monthly = models.PositiveIntegerField(
        _('Backup monthly'),
        blank=True,
        default=0,
    )

    #backup server 1
    backup_server = models.CharField(
        _('Backup server 1'),
        max_length=128,
        blank=True,
    )

    backup_user = models.CharField(
        _('Backup user 1'),
        max_length=128,
        blank=True,
    )

    #backup server 2
    backup_server_2 = models.CharField(
        _('Backup server 2'),
        max_length=128,
        blank=True,
    )

    backup_user_2 = models.CharField(
        _('Backup user 2'),
        max_length=128,
        blank=True,
    )

    #backup server old
    backup_server_old = models.CharField(
        _('Backup server old'),
        max_length=128,
        blank=True,
    )

    backup_user_old = models.CharField(
        _('Backup user old'),
        max_length=128,
        blank=True,
    )


    def __str__(self):
        return 'VM--%s' % self.name


class ConfigurationCategory(models.Model):
    """ Configuration categories """

    category = models.CharField(
        _('Categoría'),
        max_length=128,
        blank=False,
        null=True
    )

    class Meta:
        verbose_name_plural = _('categories')
        ordering = ( 'category', )

    def __str__(self):
        return self.category


class Configuration(models.Model):
    """ App family model definition """

    name = models.CharField(
        _('Nombre'),
        max_length=128,
        blank=False,
        null=True
    )
    slug = models.SlugField(
        null=True
    )
    logo_filename = models.CharField(
        _('Logo'),
        blank=True,
        help_text=_(
            'Nombre de archivo de la imagen de logo, relativa a la '
            'carpeta remota de assets definida en settings.py'
        ),
        max_length=128
    )

    class Meta:
        ordering = ( 'name', )

    @property
    def last_version(self):
         return ConfigurationVersion.objects.filter(conf=self).order_by('-version')[0]

    @property
    def logo(self):
        base_markup = '<img width="150" height="150"'
        if self.logo_filename:
            icon_markup = '%s src="%s/%s" />' % ( base_markup, settings.REMOTE_ASSETS_FOLDER, self.logo_filename )
        else:
            icon_markup = '%s src="%s/%s" />' % ( base_markup, static('/apix/img/blank-image.svg'))
        return mark_safe( icon_markup )

    def __str__(self):
        return self.name

class ConfigurationTranslation(models.Model):
    lang = models.CharField(
        _('Idioma'),
        max_length=2,
        choices=LANGUAGES
    )
    name = models.CharField(
        _('Nombre'),
        max_length=128,
        blank=False,
        null=True
    )
    link = URLOrRelativeURLField(
        _('Enlace'),
        blank=True
    )
    link_text = models.CharField(
        _('Texto del enlace'),
        blank=True,
        max_length=128,
    )
    link_target = models.BooleanField(
        _('Abrir en otra ventana'),
        default=False,
        blank=True,
        max_length=128,
    )
    summary = models.TextField(
        _('Resumen'),
        blank=True,
    )
    description = RichTextUploadingField(
        _('Descripción'),
        blank=True,
    )
    source = models.ForeignKey(
        Configuration,
        verbose_name=_('Original'),
        related_name='translations',
        on_delete=models.CASCADE
    )
    
    class Meta:
        unique_together = (('lang', 'source'),)

    def __str__(self):
        return '%s [%s]' % ( self.name, self.lang )


class User(models.Model):
    """ Users of the system """

    name = models.CharField(
        _('Nombre de usuario'),
        max_length=128,
        blank=True
    )

    class Meta:
        ordering = ( 'name', )

    def __str__(self):
        return self.name


class ConfigurationVersion(models.Model):
    """ App model definition """

    conf = models.ForeignKey(
        Configuration,
        verbose_name=_('App'),
        related_name='versions',
        null=True,
        blank=False,
        on_delete=models.CASCADE
    )
    categories = models.ManyToManyField(
        ConfigurationCategory,
        verbose_name=_('Categorías'),
        related_name='confs'
    )
    version = models.FloatField(
        _('Versión'),
        blank=False,
        default=1.0
        )
    dependencies = models.ManyToManyField(
        'self',
        blank=True,
        symmetrical=False,
        verbose_name=_('Dependencias con otras aplicaciones'),
        related_name='versions'
    )
    users = models.ManyToManyField(
        User,
        blank=True,
        verbose_name=_('Dependencias con usuarios'),
        related_name='versions'
    )
    linkapp = models.BooleanField(
        _('¿Enlace de menú?'),
        default=False,
        blank=False,
        help_text=_('¿Mostrar enlace a la app en el menú?')
    )
    tomenu = models.CharField(
        _('Sección de menú'),
        blank=True,
        max_length=36,
        choices = (
            ('app-links', _('App links')),
            ('mail-links', _('Mail links'))
        ),
        help_text=_('Selecciona sección de menu para el enlace')
    )
    remove = models.BooleanField(
        _('¿Borrar?'),
        default=False,
        blank=False,
        help_text=_('Indicar si la app se puede desinstalar')
    )
    disk = models.PositiveIntegerField(
        _('Uso disco (MB)'),
        blank=False,
        default=0
    )
    ram = models.PositiveIntegerField(
        _('Uso RAM (MB)'),
        blank=False,
        default=0
    )

    def get_absolute_url(self):
        return reverse('admin:models_configurationversion_change', args=[self.pk])

    class Meta:
        verbose_name = _('configuration version')
        verbose_name_plural = _('configuration versions')
        ordering = ( 'conf', 'version' )

    def __str__(self):
        return "%s %s" % ( self.conf.name, self.version )


class ConfigurationField(models.Model):

    widget = models.CharField(
        _('Widget'),
        choices = (
            ('select',     _('Select')),
            ('text',       _('CharField')),
            ('password',   _('PasswordField')),
            ('url',        _('URLField')),
            ('decimal',    _('DecimalField')),
            ('boolean',    _('BooleanField')),
            ('fqdn',       _('FqdnField')),
            ('fqdn_nodns', _('FqdnField (sin comprobación de DNS)'))
        ),
        max_length=24,
        default='text',
        blank=False
    )
    fid = models.CharField(
        _('Id del campo'),
        max_length=128,
        blank=False,
        null=True
    )
    conf = models.ForeignKey(
        ConfigurationVersion,
        on_delete=models.CASCADE,
        related_name='fields',
        null=True
    )
    required = models.BooleanField(
        _('Requerido'),
        default=True,
        blank=True,
        help_text=_('¿Es un campo requerido?')
    )
    required_if_installed = models.BooleanField(
        _('Requerido tras instalación'),
        default=True,
        blank=True,
        help_text=_('Marcar si el campo es requerido tras la instalación de la app')
    )
    editable = models.BooleanField(
        _('Editable'),
        default=True,
        blank=True,
        help_text=_('Marcar si el campo es editable tras la instalación')
    )
    link = models.BooleanField(
        _('Enlace a la aplicación'),
        default=True,
        blank=False,
        help_text=_('Si se trata de un campo FqdnField, marcar para que aparezca enlace a ir a la aplicación')
    )

    def __str__(self):
        default_translation = self.translations.filter(
            lang=settings.DEFAULT_API_LANGUAGE
        )
        if default_translation:
            return default_translation[0].label
        return "%s %s [%s]" % (
            self.conf.conf.name,
            self.conf.version,
            self.widget
        )

    class Meta:
        ordering = ( 'conf', )

class ConfigurationFieldTranslation(models.Model):

    source = models.ForeignKey(
        ConfigurationField,
        verbose_name=_('Campo'),
        related_name='translations',
        on_delete=models.CASCADE
    )
    lang = models.CharField(
        _('Idioma'),
        max_length=2,
        choices=LANGUAGES,
        default='es'
    )
    label = models.CharField(
        _('Etiqueta'),
        max_length=256,
        blank=False,
        null=True
    )
    default_value = models.TextField(
        _('Valor(es) por defecto'),
        blank=True,
        null=True,
        help_text=_(
            'Puedes asignar aquí el valor por defecto del campo. '
            'Si el valor es una lista usa una línea por cada opción siguiendo '
            'el formato "valor : clave"'
        )
    )
    helptext = models.TextField(
        _('Texto de ayuda'),
        blank=True,
        help_text=_(
            'Asigna aquí el texto de ayuda para la configuración de este campo.'
        )
    )
    
    class Meta:
        unique_together = (('lang', 'source'),)

    def __str__(self):
        return self.label


class ConfigurationLink(models.Model):
    """ ConfigurationLink model definition """
    
    lang = models.CharField(
        _('Idioma'),
        max_length=2,
        choices=LANGUAGES,
        default='es'
    )
    label = models.CharField(
        _('Etiqueta'),
        max_length=256,
        blank=False,
        null=True,
        help_text=_(
            'Texto del enlace.'
        )
    )
    url = URLOrRelativeURLField(
        _('URL'),
        blank=False,
        null=True,
        max_length=256,
        help_text=_(
            'Ruta relativa o absoluta del enlace.'
        )
    )
    blank = models.BooleanField(
        _('Abrir en otra ventana'),
        default=True,
        blank=True,
        max_length=128,
    )
    conf = models.ForeignKey(
        ConfigurationVersion,
        on_delete=models.CASCADE,
        related_name='links',
        null=True
    )
    
    def __str__(self):
        return self.label

class ConfigurationRemoveInfo(models.Model):
    """ ConfigurationLink model definition """
    
    lang = models.CharField(
        _('Idioma'),
        max_length=2,
        choices=LANGUAGES,
        default='es'
    )
    description = models.TextField(
        _('Descripción')
    )
    conf = models.ForeignKey(
        ConfigurationVersion,
        on_delete=models.CASCADE,
        related_name='removeinfo',
        null=True
    )

class Release(models.Model):
    """ Release model definition """

    name = models.CharField(
        _('Nombre'),
        max_length=128,
    )
    description = models.CharField(
        _('Description'),
        max_length=128,
        blank=True,
    )
    configurations = models.ManyToManyField(
        ConfigurationVersion,
        verbose_name=_('Configuraciones'),
        related_name='releases',
    )
    releases = models.ManyToManyField(
        'self',
        blank=True,
        verbose_name=_('Releases compatibles'),
    )
    updatedapps = models.ManyToManyField(
        Configuration,
        blank = True,
        verbose_name=_('Aplicaciones actualizadas'),
    )

    status = models.CharField(
        _('Estado'),
        max_length=1,
        choices = (
            ('0', _('Producción')),
            ('1', _('Desarrollo')),
        )
    )
    active = models.BooleanField(
        default=True,
    )
    order = models.PositiveSmallIntegerField(
        _('Orden')
    )

    class Meta:
        ordering = ( '-order', )

    def __str__(self):
        return self.name

    @property
    def updatedapps_name(self):
        list_app = []
        for a in self.updatedapps.all():
            list_app.append(a.slug)
        return list_app 

class ReleaseFields(models.Model):
    """ Release fields model definition """

    lang = models.CharField(
        _('Idioma'),
        max_length=2,
        choices=LANGUAGES
    )
    description = models.TextField(
        _('Descripción')
    )
    release = models.ForeignKey(
        Release,
        verbose_name=_('Release'),
        on_delete = models.CASCADE,
        related_name='fields',
    )
    
    class Meta:
        unique_together = (('lang', 'release'),)
        verbose_name = _('release fields')
        verbose_name_plural = _('release fields')

    def __str__(self):
        return 'Fields %s [%s]' % (
            self.release.name,
            self.lang
        ) 
