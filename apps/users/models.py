# django
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth.models import AbstractUser

class Uxer(AbstractUser):
    """ User model """

    hashed_token = models.TextField(
        _('Token'),
        max_length=218,
        blank=True
    )
