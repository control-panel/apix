import json
from apps.users.models import Uxer
from apps.models.models import VM
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    help = 'Import Users'

    def add_arguments(self, parser):
        parser.add_argument('--json', type=str, help='Path to json file with users')

    def handle(self, *args, **kwargs):
        jsonfile = kwargs['json']
        print ('json: ' +jsonfile)
        usersList = []
        print ("Started Reading JSON file which contains multiple JSON users")
        with open(jsonfile) as f:
          for jsonObj in f:
            print (jsonObj)
            userDict = json.loads(jsonObj)
            usersList.append(userDict)

        print ("Adding users:")
        for user in usersList:
          print (user["username"])
          #print (user["services"]["resume"]["loginTokens"][0]["hashedToken"])
          try:
            vmobj = VM.objects.get(name=user["username"])
            print ('Existe vm ' + user["username"] + ' / se intenta importar usuario')
            try:
              uxerobj = Uxer.objects.get(username=user["username"])
              print ('Ya existe usuario ' + user["username"] + ' / se descarta crear uno nuevo')
            except Uxer.DoesNotExist:
              print ('No existe usuario ' + user["username"] + ' / se añade nuevo')
              newuser = Uxer.objects.create_user(username=user["username"], email='', password=user["services"]["resume"]["loginTokens"][0]["hashedToken"])
              #add new user to vm object
              vm = VM.objects.get(name=user["username"])
              vm.users.add(newuser)
              vm.save()
          except VM.DoesNotExist:
            print ('No existe vm ' + user["username"] + ' / se descarta crear usuario')
