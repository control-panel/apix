from apps.users.models import Uxer
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    help = 'Create User'

    def add_arguments(self, parser):
        parser.add_argument('--user', type=str, help='Username')
        parser.add_argument('--password', type=str, help='Password')

    def handle(self, *args, **kwargs):
        user = kwargs['user']
        password = kwargs['password']
        print ('user: ' + user)
        print ('password: ' + password)
        #delete user if exists
        if Uxer.objects.filter(username=user).exists():
          u = Uxer.objects.get(username=user)
          u.delete()
        Uxer.objects.create_user(username=user, email='', password=password)
