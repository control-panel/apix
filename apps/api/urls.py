# django
from django.urls import path, re_path
from django.conf.urls import include
# contrib
from rest_framework import routers
# app
from . import views
# auth
from rest_framework.authtoken import views as authtoken_views

app_name = 'api'
router = routers.DefaultRouter()

router.register(
    r'vm',
    views.VMViewSet,
    basename='vm'
)
router.register(
    r'releases',
    views.ReleaseViewSet,
    basename='releases'
)

urlpatterns = [
    path(
        '',
        include(router.urls)
    ),
    path(
        '',
        include('rest_framework.urls')
    ),
    #authtoken
    path(
        'api-token-auth/',
        authtoken_views.obtain_auth_token
    ),
]
