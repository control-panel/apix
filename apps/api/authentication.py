# python
import base64, hashlib
# django
from apps.users.models import Uxer
from apps.models.models import VM
from django.http import HttpResponseForbidden
# contrib
from rest_framework import authentication
from rest_framework import exceptions


def encrypt_string(hash_string):
    token = base64.b64encode(
        hashlib.sha256(
            hash_string.encode()
        ).digest()
    )
    return token.decode('utf-8')

class ApixAuthentication(authentication.BaseAuthentication):

    def authenticate(self, request):
        request_username = request.META.get('HTTP_X_USER_ID')
        request_token    = request.META.get('HTTP_X_AUTH_TOKEN')
        print(request_username, request_token)
        if not request_username or not request_token:
            return None
        try:
            user = Uxer.objects.get(username=request_username)
            user_token = user.hashed_token
            print(encrypt_string(request_token), user_token)
            if encrypt_string(request_token) != user_token:
                return None
        except Uxer.DoesNotExist:
            raise exceptions.AuthenticationFailed('No such user')

        return (
            user,
            None
        )

class ApixTokenAuthentication(authentication.TokenAuthentication):

    def authenticate(self, request):
        #check if ip for this vm has perms
        requestip=request.META['REMOTE_ADDR']
        if 'HTTP_X_HOSTNAME' in request.META:
          hostname=request.META['HTTP_X_HOSTNAME']
          vm = VM.objects.get(name=hostname)
          if vm.ip == requestip:
            return super().authenticate(request)
