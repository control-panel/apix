# django
from django.http import HttpResponse, JsonResponse
# contrib
from rest_framework import viewsets, mixins
from rest_framework.decorators import action
# project
from apps.models import models
from . import serializers
from rest_framework.response import Response
# auth
from rest_framework.permissions import IsAuthenticated


class VMViewSet(viewsets.GenericViewSet, mixins.UpdateModelMixin, mixins.RetrieveModelMixin):
    """
    API endpoint that allows VM to be viewed or edited.
    """

    # auth
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.VMStatusSerializer
    queryset = models.VM.objects.all()
    lookup_field = 'name'
    
class ReleaseViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows VM to be viewed or edited.
    """

    #auth
    permission_classes = (IsAuthenticated,)

    queryset = models.Release.objects.all()
    lookup_field = 'name'
    serializer_class = serializers.ReleaseSerializer

    def get_serializer_context(self):
        context = super(ReleaseViewSet, self).get_serializer_context()
        context.update({
            "lang": context['request'].GET.get('lang', 'es')
        })
        return context
