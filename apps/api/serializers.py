# contrib
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.renderers import JSONRenderer
# project
from apps.models import models


class DependencySerializer(serializers.ModelSerializer):

    id = serializers.SlugRelatedField(
        source='conf',
        read_only=True,
        slug_field='slug'
    )
    name = serializers.CharField(
        source='conf.name',
        read_only=True,
    ) 

    class Meta:
        model = models.ConfigurationVersion
        fields = [
            'id',
            'name',
            'version'
        ]


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.User
        fields = [
            'name',
        ]

class FieldTranslationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ConfigurationFieldTranslation
        fields =  [
            'lang',
            'label',
            'default_value',
            'helptext'
        ]


class FieldSerializer(serializers.ModelSerializer):

    translations = FieldTranslationSerializer(many=True)

    class Meta:
        model = models.ConfigurationField
        fields = [
            'fid',
            'widget',
            'required',
            'translations'
        ]

class ConfigurationTranslationSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.ConfigurationTranslation
        fields = [
            'name',
            'link',
            'link_text',
            'link_target',
            'summary',
            'description'
        ]

class LinkSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.ConfigurationLink
        fields = [
            'lang',
            'label',
            'url',
            'blank',
        ]

class RemoveInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.ConfigurationRemoveInfo
        fields = [
            'lang',
            'description',
        ]

class ConfigurationVersionSerializer(serializers.ModelSerializer):

    users        = UserSerializer(many=True)
    dependencies = DependencySerializer(many=True)
    settings     = serializers.SerializerMethodField()
    fieldlist    = serializers.SerializerMethodField()
    links        = serializers.SerializerMethodField()
    removeinfo   = serializers.SerializerMethodField()
    resources    = serializers.SerializerMethodField()
    
    class Meta:
        model = models.ConfigurationVersion
        depth = 1
        fields = [
            'settings',
            'fieldlist',
            'users',
            'dependencies',
            'links',
            'resources',
            'removeinfo'
        ]
    
    def get_links(self, obj):
        lang = self.context.get("lang")
        links = list( map(
            lambda link : {
                'label' : link.label,
                'url'   : link.url,
                'blank' : link.blank
            }, 
            obj.links.filter(lang = lang)
        ) )
        # Add configuration links
        fields = obj.fields.filter(editable=True)
        # if object has fields
        if len( fields ) > 0:
            links.insert(0, {
                'label' : _('Configurar'),
                'url'   : '/cpanel/services/settings/%s' % obj.conf.slug,
                'blank' : False,
            })
        # Add link to app for field domain (editable or not)
        fields = obj.fields.filter()
        # if object has domain
        if any( field.fid == 'domain' and field.link == True for field in fields ):
            links.insert(0, {
                'label' : _('Ir a la aplicación'),
                'url'   : 'ldap',
                'blank' : True
            })
        return links


    def get_removeinfo(self, obj):
        lang = self.context.get("lang")
        r = obj.removeinfo.filter(lang = lang).first()
        if r:
            print(r.description)
            return {
                'description' : r.description,
            }
        
        
    def get_fieldlist(self, obj):
        field_list = []
        lang = self.context.get("lang")
        for f in obj.fields.all():
            translations = models.ConfigurationFieldTranslation.objects.filter(
                source = f, 
                lang = lang
            )
            if translations.exists(): 
                translation = translations[0]
                field = {
                    'fid'           : f.fid,
                    'widget'        : f.widget,
                    'required'      : f.required,
                    'required_if_installed'                : f.required_if_installed,
                    'editable'      : f.editable,
                    'label'         : translation.label if translation.label else '',
                    'default_value' : translation.default_value if translation.default_value else '',
                    'helptext'      : translation.helptext if translation.helptext else '',
                }
                field_list.append(field)
    
        return field_list
    
    def get_settings(self, obj):
        lang = self.context.get("lang")
        t = obj.conf.translations.get(lang=lang)
        return {
            'id' : obj.conf.slug,
            'name' : t.name,
            'remove' : obj.remove,
            'linkapp' : obj.linkapp,
            'tomenu' : obj.tomenu,
            'summary' : t.summary,
            'description' : t.description,
            'version' : obj.version,
            'logo' : obj.conf.logo_filename,
            'name' : t.name,
            'link' : t.link,
            'link_text' : t.link_text,
            'link_blank' : t.link_target,
        }

    def get_resources(self, obj):
        return {
            'disk' : obj.disk,
            'ram' : obj.ram,
        }

class ConfigurationSerializer(serializers.ModelSerializer):

    logo = serializers.CharField( source='logo_filename' )
    translations  = ConfigurationTranslationSerializer(
        many = True,
    )
    versions      = ConfigurationVersionSerializer(
        many = True
    )

    class Meta:
        model = models.Configuration
        fields = [
            'name',
            'logo',
            'translations',
            'versions'
        ]

class ReleaseFieldsSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.ReleaseFields
        fields = [
            'lang',
            'description',
        ]


class ReleaseSimpleSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Release
        fields = [
            'name',
        ]

class ReleaseSerializer(serializers.ModelSerializer):

    update = serializers.SerializerMethodField()

    configurations = ConfigurationVersionSerializer(
        many = True
    )
    description = serializers.SerializerMethodField()


    class Meta:
        model = models.Release
        fields = [
            'name',
            'updatedapps_name',
            'active',
            'status',
            'order',
            'description',
            'update',
            'configurations'
        ]
    """
    def get_updatedapps(self, obj):
        all_s = []
        for o in obj.updatedapps.all():
            all_s.append(o.name)
        return all_s
    """
    def get_update(self, obj):
        updates = obj.releases.filter(
            order__gte = obj.order
        ).order_by('order')
        lang  = self.context.get("lang")
        if(updates): 
            fields = models.ReleaseFields.objects.filter(
                release = updates[0],
                lang    = lang
            )
            return {
                'name' : updates[0].name,
                'description' : fields[0].description,
                'updatedapps' : updates[0].updatedapps_name
            }
        return False
    
    def get_description(self, obj):
        lang  = self.context.get("lang")
        field = models.ReleaseFields.objects.filter(
            release = obj,
            lang = lang
        )
        return field[0].description if field else ''

class VMStatusSerializer(serializers.ModelSerializer):

    puppetstatus = serializers.SerializerMethodField('get_status')
    backup_server = serializers.SerializerMethodField('get_backup_primary_server')
    backup_user = serializers.SerializerMethodField('get_backup_primary_user')
    backup_secondary_server = serializers.SerializerMethodField('get_backup_secondary_server')
    backup_secondary_user = serializers.SerializerMethodField('get_backup_secondary_user')
    
    def get_status(self, obj):
        return obj.get_status_display().lower()

    class Meta:
        model = models.VM
        fields = [
            'puppetstatus',
            'status',
            'backup_enabled',
            'backup_port',
            'backup_daily',
            'backup_weekly',
            'backup_monthly',
            'backup_server',
            'backup_user',
            'backup_secondary_server',
            'backup_secondary_user',
            'backup_server_old',
            'backup_user_old',
            's3_enabled',
            's3_mastodon_enabled',
            's3_mastodon_filesystem',
            's3_mastodon_bucket',
            's3_mastodon_location',
            's3_mastodon_prefix',
            's3_mastodon_usage',
            's3_mastodon_access_key',
            's3_mastodon_secret_key',
            's3_peertube_enabled',
            's3_peertube_filesystem',
            's3_peertube_bucket',
            's3_peertube_location',
            's3_peertube_prefix',
            's3_peertube_usage',
            's3_peertube_access_key',
            's3_peertube_secret_key',
        ]

    def get_backup_primary_server(self, obj):
        p = obj.backup_primary
        if p == '1':
            return obj.backup_server
        else:
            return obj.backup_server_2

    def get_backup_primary_user(self, obj):
        p = obj.backup_primary
        if p == '1':
            return obj.backup_user
        else:
            return obj.backup_user_2

    def get_backup_secondary_server(self, obj):
        p = obj.backup_primary
        if p == '1':
            return obj.backup_server_2
        else:
            return obj.backup_server

    def get_backup_secondary_user(self, obj):
        p = obj.backup_primary
        if p == '1':
            return obj.backup_user_2
        else:
            return obj.backup_user

class VMSerializer(serializers.ModelSerializer):

    release = ReleaseSerializer()

    class Meta:
        model = models.VM
        fields = [
            'release'
        ]
