import os
from .settings import BASE_DIR, TEMPLATES

# ALLOWED HOSTS
ALLOWED_HOSTS = [
    'localhost'
]

# ALLOWED IPS
ALLOWED_IPS = [
    '127.0.0.1',
]

# FLAGS
DEBUG = True
DEBUG_JS = True
DEBUG_CSS = True

# SECRET_KEY
SECRET_KEY = 'secretkey'

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# USER MODEL

AUTH_USER_MODEL = 'users.Uxer'
