#django
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from django.views.generic import TemplateView
from django.conf.urls.static import static
# project
from apps.api import urls as api_urls
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),

    # API
    # v1.0
    path(
        'api/1.0/',
        include(api_urls, namespace="api")
    ),
    path(
        '',
        TemplateView.as_view(
            template_name='pages/front.html'
        ),
        name='home'
    ),

    # ckeditor urls
    path(
        r'ckeditor/',
        include('ckeditor_uploader.urls')
    ),
]

# Add static URLS when running a standalone server through manage.py
if settings.DEBUG == True:
   urlpatterns += static( settings.STATIC_URL, document_root = settings.STATIC_ROOT )
   urlpatterns += static( settings.MEDIA_URL,  document_root = settings.MEDIA_ROOT )
