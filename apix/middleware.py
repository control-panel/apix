from django.conf import settings
from django import http

class IPBlockerMiddleware(object):

    def __init__(self, get_response):
            self.get_response = get_response

    def __call__(self, request):
        if request.path.startswith('/admin') and request.META['REMOTE_ADDR'] not in settings.ALLOWED_IPS:
           return http.HttpResponseForbidden('<h1>Forbidden</h1>')
        return self.get_response(request)
